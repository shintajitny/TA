package id.sch.smktelkom_mlg.barangkuta.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import id.sch.smktelkom_mlg.barangkuta.Model.BarangModel;
import id.sch.smktelkom_mlg.barangkuta.R;

public class BarangAdapter extends RecyclerView.Adapter<BarangAdapter.ViewHolder> {
    ArrayList<BarangModel> barangModels = new ArrayList<BarangModel>();

    public BarangAdapter(ArrayList<BarangModel> barangModels) {
        this.barangModels = barangModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("viewholder", "onCreateViewHolder: ");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        Log.d("viewholder", "onCreateViewHolder: ");

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvNamaBarang.setText(barangModels.get(position).namaBarang);
        holder.tvTanggal.setText(barangModels.get(position).tanggalBarang);
        holder.tvKelas.setText(barangModels.get(position).kelasBarang);
    }

    @Override
    public int getItemCount() {
        return barangModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvNamaBarang, tvTanggal, tvKelas;

        public ViewHolder(View itemView) {
            super(itemView);

            tvNamaBarang = itemView.findViewById(R.id.tvNamaBarang);
            tvTanggal = itemView.findViewById(R.id.tvTanggal);
            tvKelas = itemView.findViewById(R.id.tvKelas);
        }
    }
}
