package id.sch.smktelkom_mlg.barangkuta;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class tambahbarangActivity extends AppCompatActivity {
    EditText NamaBarang;
    Spinner SpinnerKelas;
    EditText Tanggal;
    EditText Punishment;
    Button BtnPost;
    //ImageView   TambahFoto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambahbarang);


        NamaBarang = findViewById(R.id.NamaBarang);
        SpinnerKelas = findViewById(R.id.spinnerKelas);
        Tanggal = findViewById(R.id.Tanggal);
        Punishment = findViewById(R.id.Punishment);
        //TambahFoto  = findViewById(R.id.TambahFoto);
        BtnPost = findViewById(R.id.BtnPost);


        BtnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addBarang();
                Intent intent = new Intent(tambahbarangActivity.this, TampilBarangActivity.class);
                startActivity(intent);
            }
        });
    }

    public void addBarang() {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference dbRef = firebaseDatabase.getReference();
        Barang barang = new Barang(NamaBarang.getText().toString(),
                SpinnerKelas.getSelectedItem().toString(),
                Tanggal.getText().toString(),
                Punishment.getText().toString());
        dbRef.push().setValue(barang);

        finish();
    }


}
