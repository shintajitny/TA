package id.sch.smktelkom_mlg.barangkuta.Model;

/**
 * Created by SHINTA J on 03/05/2018.
 */

public class BarangModel {
    public String kelasBarang, namaBarang, punishmentBarang, tanggalBarang;

    public BarangModel(String kelasBarang, String namaBarang, String punishmentBarang, String tanggalBarang) {
        this.kelasBarang = kelasBarang;
        this.namaBarang = namaBarang;
        this.punishmentBarang = punishmentBarang;
        this.tanggalBarang = tanggalBarang;
    }

    public BarangModel() {
    }
}
