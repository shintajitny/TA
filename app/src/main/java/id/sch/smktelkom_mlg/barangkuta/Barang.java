package id.sch.smktelkom_mlg.barangkuta;

/**
 * Created by SHINTA J on 02/05/2018.
 */

public class Barang {
    String Nama;
    String Kelas;
    String Tanggal;
    String Punishment;
    //String FotoBarang;


    public Barang(String nama, String kelas, String tanggal, String punishment) {
        Nama = nama;
        Kelas = kelas;
        Tanggal = tanggal;
        Punishment = punishment;
        //FotoBarang = fotobarang;

    }

    public String getNamaBarang() {
        return Nama;
    }

    public String getKelasBarang() {
        return Kelas;
    }

    public String getTanggalBarang() {
        return Tanggal;
    }

    public String getPunishmentBarang() {
        return Punishment;
    }
}
