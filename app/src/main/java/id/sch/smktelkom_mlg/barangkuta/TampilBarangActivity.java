
package id.sch.smktelkom_mlg.barangkuta;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import id.sch.smktelkom_mlg.barangkuta.Adapter.BarangAdapter;
import id.sch.smktelkom_mlg.barangkuta.Model.BarangModel;

public class TampilBarangActivity extends AppCompatActivity {
    ArrayList<BarangModel> barangModels = new ArrayList<BarangModel>();
    BarangAdapter mAdapter;
    DatabaseReference database;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tampil_barang);
        recyclerView = findViewById(R.id.recycler);
        getDataFirebase();
    }

    private void getDataFirebase() {
        Log.d("BarankuTaLog", "onDataChange: Start Activity");
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference dbRef = firebaseDatabase.getReference();
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("BarankuTaLog", "onDataChange: " + dataSnapshot.toString());
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    BarangModel barangModel = dataSnapshot1.getValue(BarangModel.class);
//                    barangModel.kelasBarang = dataSnapshot1.child("kelasBarang").getValue().toString();
//                    barangModel.namaBarang = dataSnapshot1.child("namaBarang").getValue().toString();
//                    barangModel.punishmentBarang =dataSnapshot1.child("punishmentBarang").getValue().toString();
//                    barangModel.tanggalBarang = dataSnapshot1.child("tanggalBarang").getValue().toString();
                    barangModels.add(barangModel);
                    Log.d("firebase", "onDataChange: " + barangModel.kelasBarang);
                }
                createRecycler();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("BarankuTaLog", "onDataChange: Something error");
            }
        });
    }

    private void createRecycler() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        Log.d("data", "createRecycler: " + recyclerView);
        BarangAdapter adapter = new BarangAdapter(barangModels);
        recyclerView.setAdapter(adapter);
    }
}

